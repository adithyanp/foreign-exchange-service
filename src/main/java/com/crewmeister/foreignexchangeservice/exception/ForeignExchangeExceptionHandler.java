package com.crewmeister.foreignexchangeservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import java.net.MalformedURLException;

@ControllerAdvice
public class ForeignExchangeExceptionHandler {

  @ExceptionHandler(value = {RuntimeException.class, MalformedURLException.class})
  public ResponseEntity<ForeignExchangeExceptionResponse> handleRuntimeException(
      Exception exception, WebRequest webRequest) {
    return ResponseEntity.internalServerError()
        .body(
            ForeignExchangeExceptionResponse.builder()
                .status(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .errorMessage(exception.getMessage())
                .path(webRequest.getContextPath())
                .build());
  }
}
