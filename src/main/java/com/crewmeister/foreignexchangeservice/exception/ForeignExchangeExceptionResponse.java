package com.crewmeister.foreignexchangeservice.exception;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
@AllArgsConstructor
public class ForeignExchangeExceptionResponse {
  private int status;
  private String errorMessage;
  private String path;
}
