package com.crewmeister.foreignexchangeservice.rest;

import com.crewmeister.foreignexchangeservice.model.Currency;
import com.crewmeister.foreignexchangeservice.model.ForeignExchangeResponse;
import com.crewmeister.foreignexchangeservice.port.RequestForeignExchange;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/currencies")
@RequiredArgsConstructor
public class ForeignExchangeResource {

  private final RequestForeignExchange requestForeignExchange;

  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<ForeignExchangeResponse> getForeignExchange(
      @RequestParam(value = "forCurrency", required = false) Currency forCurrency,
      @RequestParam(value = "forDates", required = false) List<String> forDates,
      @RequestParam(value = "forDay", required = false) String forDay,
      @RequestParam(value = "amount", required = false) Double amount)
      throws Exception {
    return ResponseEntity.ok(
        requestForeignExchange.getForeignExchange(forCurrency, forDates, forDay, amount));
  }
}
