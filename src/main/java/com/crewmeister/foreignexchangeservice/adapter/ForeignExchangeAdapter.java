package com.crewmeister.foreignexchangeservice.adapter;

import com.crewmeister.foreignexchangeservice.model.Currency;
import com.crewmeister.foreignexchangeservice.model.ForeignExchange;
import com.crewmeister.foreignexchangeservice.port.RequestForeignExchangeAdapter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class ForeignExchangeAdapter implements RequestForeignExchangeAdapter {
  @Override
  public List<ForeignExchange> getForeignExchange(Currency currency) throws IOException {
    List<ForeignExchange> foreignExchangeList = new ArrayList<>();
    var url = new URL(currency.statistics_url);
    System.out.println("fetching data from url :" + url.getPath());
    try (InputStream is = url.openStream()) {

      var bufferedReader = new BufferedReader(new InputStreamReader(is));
      var i = 1;

      while (bufferedReader.read() != -1) {
        i++;
        var line = bufferedReader.readLine();
        if (i > 10) {
          var values = line.split(",");
          var foreignExchange = ForeignExchange.builder().date(values[0]).value(values[1]).build();
          foreignExchangeList.add(foreignExchange);
        }
      }
    }
    return foreignExchangeList;
  }
}
