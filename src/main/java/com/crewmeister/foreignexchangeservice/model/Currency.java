package com.crewmeister.foreignexchangeservice.model;

public enum Currency {
    AUD("https://api.statistiken.bundesbank.de/rest/download/BBEX3/D.AUD.EUR.BB.AC.000?format=csv&lang=en"),
    BGN("https://api.statistiken.bundesbank.de/rest/download/BBEX3/D.BGN.EUR.BB.AC.000?format=csv&lang=en"),
    BRL("https://api.statistiken.bundesbank.de/rest/download/BBEX3/D.BRL.EUR.BB.AC.000?format=csv&lang=en"),
    CAD("https://api.statistiken.bundesbank.de/rest/download/BBEX3/D.CAD.EUR.BB.AC.000?format=csv&lang=en"),
    CHF("https://api.statistiken.bundesbank.de/rest/download/BBEX3/D.CHF.EUR.BB.AC.000?format=csv&lang=en"),
    CNY("https://api.statistiken.bundesbank.de/rest/download/BBEX3/D.CNY.EUR.BB.AC.000?format=csv&lang=en"),
    CYP("https://api.statistiken.bundesbank.de/rest/download/BBEX3/D.CYP.EUR.BB.AC.000?format=csv&lang=en"),
    CZK("https://api.statistiken.bundesbank.de/rest/download/BBEX3/D.CZK.EUR.BB.AC.000?format=csv&lang=en"),
    DKK("https://api.statistiken.bundesbank.de/rest/download/BBEX3/D.DKK.EUR.BB.AC.000?format=csv&lang=en"),
    EEK("https://api.statistiken.bundesbank.de/rest/download/BBEX3/D.EEK.EUR.BB.AC.000?format=csv&lang=en"),
    GBP("https://api.statistiken.bundesbank.de/rest/download/BBEX3/D.GBP.EUR.BB.AC.000?format=csv&lang=en"),
    GRD("https://api.statistiken.bundesbank.de/rest/download/BBEX3/D.GRD.EUR.BB.AC.000?format=csv&lang=en"),
    HKD("https://api.statistiken.bundesbank.de/rest/download/BBEX3/D.HKD.EUR.BB.AC.000?format=csv&lang=en"),
    HRK("https://api.statistiken.bundesbank.de/rest/download/BBEX3/D.HRK.EUR.BB.AC.000?format=csv&lang=en"),
    HUF("https://api.statistiken.bundesbank.de/rest/download/BBEX3/D.HUF.EUR.BB.AC.000?format=csv&lang=en"),
    IDR("https://api.statistiken.bundesbank.de/rest/download/BBEX3/D.IDR.EUR.BB.AC.000?format=csv&lang=en"),
    ILS("https://api.statistiken.bundesbank.de/rest/download/BBEX3/D.ILS.EUR.BB.AC.000?format=csv&lang=en"),
    INR("https://api.statistiken.bundesbank.de/rest/download/BBEX3/D.INR.EUR.BB.AC.000?format=csv&lang=en"),
    ISK("https://api.statistiken.bundesbank.de/rest/download/BBEX3/D.ISK.EUR.BB.AC.000?format=csv&lang=en"),
    JPY("https://api.statistiken.bundesbank.de/rest/download/BBEX3/D.JPY.EUR.BB.AC.000?format=csv&lang=en"),
    KRW("https://api.statistiken.bundesbank.de/rest/download/BBEX3/D.KRW.EUR.BB.AC.000?format=csv&lang=en"),
    LTL("https://api.statistiken.bundesbank.de/rest/download/BBEX3/D.LTL.EUR.BB.AC.000?format=csv&lang=en"),
    MTL("https://api.statistiken.bundesbank.de/rest/download/BBEX3/D.MTL.EUR.BB.AC.000?format=csv&lang=en"),
    MXM("https://api.statistiken.bundesbank.de/rest/download/BBEX3/D.MXN.EUR.BB.AC.000?format=csv&lang=en"),
    MYR("https://api.statistiken.bundesbank.de/rest/download/BBEX3/D.MYR.EUR.BB.AC.000?format=csv&lang=en"),
    NOK("https://api.statistiken.bundesbank.de/rest/download/BBEX3/D.NOK.EUR.BB.AC.000?format=csv&lang=en"),
    NZD("https://api.statistiken.bundesbank.de/rest/download/BBEX3/D.NZD.EUR.BB.AC.000?format=csv&lang=en"),
    PHP("https://api.statistiken.bundesbank.de/rest/download/BBEX3/D.PHP.EUR.BB.AC.000?format=csv&lang=en"),
    PLN("https://api.statistiken.bundesbank.de/rest/download/BBEX3/D.PLN.EUR.BB.AC.000?format=csv&lang=en"),
    ROL("https://api.statistiken.bundesbank.de/rest/download/BBEX3/D.ROL.EUR.BB.AC.000?format=csv&lang=en"),
    RON("https://api.statistiken.bundesbank.de/rest/download/BBEX3/D.RON.EUR.BB.AC.000?format=csv&lang=en"),
    RUB("https://api.statistiken.bundesbank.de/rest/download/BBEX3/D.RUB.EUR.BB.AC.000?format=csv&lang=en"),
    SGD("https://api.statistiken.bundesbank.de/rest/download/BBEX3/D.SGD.EUR.BB.AC.000?format=csv&lang=en"),
    SEK("https://api.statistiken.bundesbank.de/rest/download/BBEX3/D.SEK.EUR.BB.AC.000?format=csv&lang=en"),
    SIT("https://api.statistiken.bundesbank.de/rest/download/BBEX3/D.SIT.EUR.BB.AC.000?format=csv&lang=en"),
    SKK("https://api.statistiken.bundesbank.de/rest/download/BBEX3/D.SKK.EUR.BB.AC.000?format=csv&lang=en"),
    THB("https://api.statistiken.bundesbank.de/rest/download/BBEX3/D.THB.EUR.BB.AC.000?format=csv&lang=en"),
    TRL("https://api.statistiken.bundesbank.de/rest/download/BBEX3/D.TRL.EUR.BB.AC.000?format=csv&lang=en"),
    TRY("https://api.statistiken.bundesbank.de/rest/download/BBEX3/D.TRY.EUR.BB.AC.000?format=csv&lang=en"),
    USD("https://api.statistiken.bundesbank.de/rest/download/BBEX3/D.USD.EUR.BB.AC.000?format=csv&lang=en"),
    ZAR("https://api.statistiken.bundesbank.de/rest/download/BBEX3/D.ZAR.EUR.BB.AC.000?format=csv&lang=en");

    public final String statistics_url;

    Currency(String statistics_url) {
        this.statistics_url = statistics_url;
    }
}
