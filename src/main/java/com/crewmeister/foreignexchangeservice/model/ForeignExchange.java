package com.crewmeister.foreignexchangeservice.model;

import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ForeignExchange {
  private String date;
  private String value;
}
