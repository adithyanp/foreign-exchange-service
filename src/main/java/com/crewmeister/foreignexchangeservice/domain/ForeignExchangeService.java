package com.crewmeister.foreignexchangeservice.domain;

import com.crewmeister.foreignexchangeservice.model.Currency;
import com.crewmeister.foreignexchangeservice.model.ForeignExchange;
import com.crewmeister.foreignexchangeservice.model.ForeignExchangeResponse;
import com.crewmeister.foreignexchangeservice.port.RequestForeignExchange;
import com.crewmeister.foreignexchangeservice.port.RequestForeignExchangeAdapter;
import lombok.RequiredArgsConstructor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class ForeignExchangeService implements RequestForeignExchange {

  private final RequestForeignExchangeAdapter requestForeignExchangeAdapter;

  @Override
  public ForeignExchangeResponse getForeignExchange(
      Currency forCurrency, List<String> forDates, String forDay, Double amount)
      throws IOException {

    return findWhichQueryTobeExecuted(forCurrency, forDates, forDay, amount);
  }

  private ForeignExchangeResponse findWhichQueryTobeExecuted(
      Currency forCurrency, List<String> forDates, String forDay, Double amount)
      throws IOException {
    var foreignExchangeResponseBuilder = ForeignExchangeResponse.builder();
    foreignExchangeResponseBuilder.currency(forCurrency);
    if (Objects.isNull(forCurrency) && Objects.isNull(forDates) && Objects.isNull(forDay)) {
      foreignExchangeResponseBuilder.currencies(getAllAvailableCurrencies());
    }

    if (Objects.nonNull(forCurrency)) {
      if (Objects.nonNull(forDay)) {
        var foreignExchanges =
            getForeignExchangeRatesForGivenCurrencyAndDates(forCurrency, List.of(forDay));
        foreignExchangeResponseBuilder.foreignExchangeList(foreignExchanges);
        if (Objects.nonNull(amount) && amount > 0) {
          var exchangeAmount = Double.parseDouble(foreignExchanges.get(0).getValue()) * amount;
          foreignExchangeResponseBuilder.foreignExchangeAmount(exchangeAmount);
        }
      } else if (Objects.nonNull(forDates) && forDates.size() > 0) {
        foreignExchangeResponseBuilder.foreignExchangeList(
            getForeignExchangeRatesForGivenCurrencyAndDates(forCurrency, forDates));
      } else {
        foreignExchangeResponseBuilder.foreignExchangeList(
            requestForeignExchangeAdapter.getForeignExchange(forCurrency));
      }
    }
    return foreignExchangeResponseBuilder.build();
  }

  private static List<String> getAllAvailableCurrencies() {
    return Arrays.stream(Currency.values()).map(Enum::toString).collect(Collectors.toList());
  }

  private List<ForeignExchange> getForeignExchangeRatesForGivenCurrencyAndDates(
      Currency forCurrency, List<String> dates) throws IOException {
    return requestForeignExchangeAdapter.getForeignExchange(forCurrency).stream()
        .filter(
            foreignExchange ->
                dates.stream().anyMatch(date -> foreignExchange.getDate().equalsIgnoreCase(date)))
        .collect(Collectors.toList());
  }
}
