package com.crewmeister.foreignexchangeservice.port;

import com.crewmeister.foreignexchangeservice.model.Currency;
import com.crewmeister.foreignexchangeservice.model.ForeignExchange;
import com.crewmeister.foreignexchangeservice.model.ForeignExchangeResponse;

import java.io.IOException;
import java.util.List;

public interface RequestForeignExchange {
  ForeignExchangeResponse getForeignExchange(
          Currency forCurrency, List<String> forDates, String forDay, Double amount) throws IOException;
}
