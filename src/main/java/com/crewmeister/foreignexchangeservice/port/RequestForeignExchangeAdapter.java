package com.crewmeister.foreignexchangeservice.port;

import com.crewmeister.foreignexchangeservice.model.Currency;
import com.crewmeister.foreignexchangeservice.model.ForeignExchange;

import java.io.IOException;
import java.util.List;

public interface RequestForeignExchangeAdapter {

  List<ForeignExchange> getForeignExchange(Currency currency) throws IOException;
}
