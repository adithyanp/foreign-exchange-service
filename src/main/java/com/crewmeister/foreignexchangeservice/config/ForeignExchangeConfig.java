package com.crewmeister.foreignexchangeservice.config;

import com.crewmeister.foreignexchangeservice.adapter.ForeignExchangeAdapter;
import com.crewmeister.foreignexchangeservice.domain.ForeignExchangeService;
import com.crewmeister.foreignexchangeservice.port.RequestForeignExchange;
import com.crewmeister.foreignexchangeservice.port.RequestForeignExchangeAdapter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ForeignExchangeConfig {

  @Bean
  public RequestForeignExchangeAdapter getRequestForeignExchangeAdapter() {
    return new ForeignExchangeAdapter();
  }

  @Bean
  public RequestForeignExchange getRequestForeignExchange(
      RequestForeignExchangeAdapter foreignExchangeAdapter) {
    return new ForeignExchangeService(foreignExchangeAdapter);
  }
}
